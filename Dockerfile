FROM logstash:2.1

COPY conf/* /etc/logstash/conf.d/

RUN set -x \
	&& plugin update logstash-input-beats \
	&& plugin update logstash-codec-json \
	&& plugin update logstash-filter-mutate \
	&& plugin update logstash-output-elasticsearch

CMD ["logstash", "agent", "-f", "/etc/logstash/conf.d"]
